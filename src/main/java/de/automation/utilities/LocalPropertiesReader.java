package de.automation.utilities;

/**
 * @author Riffat Shahzad
 */

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.commons.lang3.StringUtils;

public class LocalPropertiesReader {

    private static int count = 0;
    private static PropertiesConfiguration properties = null;

    private static PropertiesConfiguration initializePropertiesFiles() {
        try {
            //TODO REMOVE USER DIR ACCESS
            String filePath = LocalPropertiesReader.class.getResource("/configfiles/local.properties").getPath();
            properties = new PropertiesConfiguration(filePath);
            FileChangedReloadingStrategy strategy = new FileChangedReloadingStrategy();
            strategy.setRefreshDelay(500);
            properties.setReloadingStrategy(strategy);
            properties.reload();
        }
        catch (ConfigurationException e) {
            e.printStackTrace();
        }
        return properties;
    }

    private static String getProperty(String key) {
        properties = initializePropertiesFiles();
        String value = System.getProperty(key);
        if (value != null && !StringUtils.isEmpty(value)) {
            return value;
        }
        else {
            value = properties.getString(key);
        }
        return value;
    }

    public static String getExecutionMode() {
        return getProperty("execution.mode");
    }

    public static String getUrl() {
        return getProperty("url");
    }

    public static String getGridUrl() {
        return getProperty("grid");
    }

    public static String getRetryMode() {
        return getProperty("retry.mode");
    }

    public static Boolean getQuitDriverMode() {
        return Boolean.valueOf(getProperty("quit.driver"));
    }
}
