package de.automation.utilities;

/**
 * @author Riffat Shahzad
 */

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.google.common.util.concurrent.Uninterruptibles;

class DesiredCapabilitiesFactory {

    public static DesiredCapabilities makeDesiredCapabilitiesBrowsers(String browserName) {
        DesiredCapabilities cap;
        if (browserName.equalsIgnoreCase("firefox")) {
            cap = DesiredCapabilities.firefox();
            System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
            System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "/dev/null");
            cap.setCapability("marionette", true);
            cap.setBrowserName("firefox");
            cap.setCapability("browser", "Firefox");
            cap.setCapability("browser_version", "60.0");
            cap.setCapability("os", "Windows");
            cap.setCapability("os_version", "10");
            cap.setCapability("resolution", "1920x1080");
        }
        else if (browserName.equalsIgnoreCase("chrome")) {
            cap = DesiredCapabilities.chrome();
            cap.setCapability("browserstack.local", "true");
            cap.setCapability(ChromeOptions.CAPABILITY, createChromeOptions());
        }
        else if (browserName.equalsIgnoreCase("edge")) {
            cap = DesiredCapabilities.edge();
            cap.setCapability("browser", "Edge");
            cap.setCapability("browser_version", "16.0");
            cap.setCapability("os", "Windows");
            cap.setCapability("os_version", "10");
            cap.setCapability("resolution", "1920x1080");
        }
        else if (browserName.equalsIgnoreCase("ie")) {
            cap = DesiredCapabilities.internetExplorer();
            cap.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
            Uninterruptibles.sleepUninterruptibly(5, TimeUnit.SECONDS);
            System.setProperty("webdriver.ie.driver.logfile", "DEBUG");
        }
        else if (browserName.equalsIgnoreCase("safari")) {
            cap = DesiredCapabilities.safari();
            cap.setCapability("browser", "Safari");
            cap.setCapability("resolution", "1920x1080");
        }
        else {
            throw new RuntimeException("The provided browser type: " + browserName + " is not supported yet");
        }
        cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        cap.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
        cap.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, true);
        cap.setCapability("idleTimeout", 720);
        cap.setCapability("screenResolution", "1920x1080");
        return cap;
    }

    public static ChromeOptions createChromeOptions() {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--disable-xss-auditor");
        chromeOptions.addArguments("--ignore-certificate-errors");
        chromeOptions.addArguments("--disable-infobars");
        chromeOptions.addArguments("--disable-extensions");
        chromeOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        chromeOptions.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, true);
        chromeOptions.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
        chromeOptions.setCapability("idleTimeout", 720);
        chromeOptions.setCapability("screenResolution", "1920x1080");
        return chromeOptions;
    }

    public static EdgeOptions createEdgeOptions() {
        EdgeOptions options = new EdgeOptions();
        options.setCapability("screenResolution", "1920x1080");
        options.setCapability("idleTimeout", 720);
        options.setCapability("driver.version", "44.17763.1.0");
        return options;
    }
}
