package de.automation.utilities;

public enum OS {
    WINDOWS, LINUX, MAC, SOLARIS;

    public static OS getOS() {
        OS os;
        String operSys = System.getProperty("os.name").toLowerCase();
        if (operSys.contains("win")) {
            os = OS.WINDOWS;
        }
        else if (operSys.contains("nix") || operSys.contains("nux")
                || operSys.contains("aix")) {
            os = OS.LINUX;
        }
        else if (operSys.contains("mac")) {
            os = OS.MAC;
        }
        else if (operSys.contains("sunos")) {
            os = OS.SOLARIS;
        }
        else {
            throw new IllegalArgumentException("Unresolvable OS");
        }
        return os;
    }
}
