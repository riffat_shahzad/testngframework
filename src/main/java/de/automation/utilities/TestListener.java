package de.automation.utilities;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.Reporter;

import io.qameta.allure.Allure;
import io.qameta.allure.Attachment;
import io.qameta.allure.Step;

public class TestListener implements ITestListener {
    public static final String LOG_BASE_FOLDER = "build/reports/" + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
    private static Logger log = LoggerFactory.getLogger(TestListener.class);
    private final boolean retryMode = Boolean.valueOf(LocalPropertiesReader.getRetryMode());

    protected static void createScreenshot(String testMethodName, ITestResult testResult, boolean attachToReport) {
        Object x = testResult.getInstance();
        BaseTest currentCase = (BaseTest) x;
        WebDriver driver = currentCase.getDriverInstance();
        savePNG(testMethodName, driver, attachToReport);
    }

    protected static void createScreenshot(String testMethodName, WebDriver driver, boolean attachToReport) {
        savePNG(testMethodName, driver, attachToReport);
    }

    protected static void savePNG(final String testMethodName, WebDriver driver, boolean attachToReport) {
        File destFile;
        File srcFile;

        String executionMode = LocalPropertiesReader.getExecutionMode();
        String fileName = testMethodName + ".png";
        System.setProperty("org.uncommons.reportng.escape-output", "false");
        if (driver != null) {
            if (executionMode.equals("local")) {
                srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            }
            else {
                WebDriver augmentedDriver = new Augmenter().augment(driver);
                srcFile = ((TakesScreenshot) augmentedDriver).getScreenshotAs(OutputType.FILE);
            }

            destFile = new File(LOG_BASE_FOLDER + "/screenshots", fileName);
            try {
                FileUtils.copyFile(srcFile, destFile);
            }
            catch (IOException e) {
            }
            Allure.addAttachment(driver.getCurrentUrl() + "/UrlEnd:" + testMethodName, new ByteArrayInputStream(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES)));
            Reporter.setEscapeHtml(false);
            if (attachToReport) {
                prepareLinkLocationForReporting(destFile, "screenshot");
            }
        }
    }

    public static String prepareLinkLocationForReporting(final File destFile, String linkType) {
        String url = null;

        if (LocalPropertiesReader.getExecutionMode().equals("grid")) {
            try {
                String absoluteFileLocation = destFile.getAbsolutePath();
                String repfileLocation = absoluteFileLocation.replace(System.getenv("WORKSPACE"), "artifact");
                url = System.getenv("BUILD_URL") + repfileLocation;
            }
            catch (NullPointerException e) {
            }
        }
        else {
            String workingDir = System.getProperty("user.dir");
            String projectAbsolutePath = workingDir.substring(workingDir.lastIndexOf("/") + 1, workingDir.length());
            url = destFile.getAbsolutePath().replace(workingDir, "/" + projectAbsolutePath);
        }
        if (linkType.equalsIgnoreCase("logfile")) {
            Reporter.log("Here is the complete log of executed test case:<br></br>");
            Reporter.log("<a href=" + url + ">Click link to view complete log file</a><br></br>");
        }
        else if (linkType.equalsIgnoreCase("screenshot")) {
            Reporter.log("Captured screen shot with following name: " + destFile.getName() + "<br></br>");
            Reporter.log("<a href=" + url + ">Click link to view the screenshot</a><br></br>");
        }
        return url;
    }

    @Attachment(value = "Expand here to view the Logs: {0}", type = "text/plain")
    private static byte[] saveTextLog(String fileName) {
        Allure.addDescription("For more Details. See the attached screenshot and log file in Test body");
        String content = null;
        try {
            content = new String(Files.readAllBytes(Paths.get(fileName)));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return content.getBytes();
    }

    @Step("{0}")
    public static void logToReport(String message) {
        log.info(message);
    }

    @Override
    public synchronized void onTestSkipped(ITestResult testResult) {
        log.debug("Test was skipped");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult testResult) {
        log.debug("Entering onTestFailedButWithinSuccessPercentage");
    }

    @Override
    public void onFinish(ITestContext context) {
        log.debug("Entering onFinish");
        Iterator<ITestResult> failedTestCases = context.getFailedTests().getAllResults().iterator();
        while (failedTestCases.hasNext()) {
            ITestResult failedTest = failedTestCases.next();
            ITestNGMethod failedMethod = failedTest.getMethod();

            Collection<ITestNGMethod> passedMethodsCollection = context.getPassedTests().getAllMethods();

            for (ITestNGMethod pass : passedMethodsCollection) {
                if (pass.getMethodName().equals(failedMethod.getMethodName())) {
                    failedTestCases.remove();
                }
            }

            while (context.getFailedTests().getResults(failedMethod).size() > 1) {
                failedTestCases.remove();
            }
        }
    }

    @Override
    public void onStart(ITestContext context) {
        log.debug("Entering onStart");
        if (retryMode) {
            log.debug("Setting retry");
            for (ITestNGMethod method : context.getAllTestMethods()) {
                method.setRetryAnalyzer(new RetryAnalyzer());
            }
        }
    }

    @Override
    public synchronized void onTestFailure(ITestResult testResult) {
        log.debug("Entering onTestFailure");
        String testMethodName = MDC.get("testMethodName");
        String logFileName = LOG_BASE_FOLDER + "/separateLogsTestCases/" + testMethodName + ".log";
        if (testResult.getMethod().getRetryAnalyzer() != null) {
            RetryAnalyzer retryAnalyzer = (RetryAnalyzer) testResult.getMethod().getRetryAnalyzer();
            if (retryAnalyzer.isRetryAvailable()) {
                log.info("Entering on retrying test failure method");
            }
            else {
                log.info("Entering 3rd time on test failure");
                createScreenshot(testMethodName, testResult, true);
                log.error("Error Stacktrace: ", testResult.getThrowable());
                endTestCaseWithFailedStatus(testMethodName);
                prepareLinkLocationForReporting(new File(logFileName), "logfile");
                saveTextLog(logFileName);
            }
        }
        else {
            log.error("Error Stacktrace: ", testResult.getThrowable());
            endTestCaseWithFailedStatus(testMethodName);
            createScreenshot(testMethodName, testResult, true);
            prepareLinkLocationForReporting(new File(logFileName), "logfile");
            saveTextLog(logFileName);
        }
    }

    @Override
    public synchronized void onTestStart(ITestResult arg0) {
        String testMethodName = arg0.getMethod().getMethodName() + "_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH.mm.SS"));
        log.debug("Entering onTestStart log file: " + testMethodName);
        MDC.put("testMethodName", testMethodName);
        startTestCase(testMethodName);
        log.info("Log has been started at " + new SimpleDateFormat("yyyyMMddHHmm").format(new Date()));
    }

    @Override
    public synchronized void onTestSuccess(ITestResult arg0) {
        String testMethodName = MDC.get("testMethodName");
        log.debug("Entering onTestSuccess");
        endTestCaseWithPassedStatus(testMethodName);
        createScreenshot(testMethodName, arg0, true);
        String fileName = LOG_BASE_FOLDER + "/separateLogsTestCases/" + testMethodName + ".log";
        prepareLinkLocationForReporting(new File(fileName), "logfile");
        saveTextLog(fileName);
    }

    protected void startTestCase(String testMethodName) {
        log.info("\n");
        log.info("**********************************************************************************************");
        log.info("Start of " + testMethodName + " Test Case execution!");
        log.info("**********************************************************************************************");
        log.info("\n");
    }

    protected void endTestCaseWithPassedStatus(String testMethodName) {
        log.info("\n");
        log.info("**********************************************************************************************");
        log.info("End of " + testMethodName + " Test Case execution as Passed");
        log.info("**********************************************************************************************");
        log.info("\n");
    }

    protected void endTestCaseWithFailedStatus(String testMethodName) {
        log.info("\n");
        log.info("**********************************************************************************************");
        log.info("End of " + testMethodName + " Test Case execution as Failed");
        log.info("**********************************************************************************************");
        log.info("\n");
    }
}
