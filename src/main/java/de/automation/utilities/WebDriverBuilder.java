package de.automation.utilities;

/**
 * @author Riffat Shahzad
 */

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.events.WebDriverEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;


public class WebDriverBuilder {

    public final static int IMPLICIT_WAIT_TIME = 30;
    private final static int PAGE_LOAD_WAIT_TIME = 120;
    private final static int SCRIPT_WAIT_TIME = 60;
    private static final String PROPERTY_NAME_CHROME = "webdriver.chrome.driver";
    private static final String PROPERTY_NAME_EDGE = "webdriver.edge.driver";
    private static final String PROPERTY_NAME_IE = "webdriver.ie.driver";
    private static final String PROPERTY_NAME_GECKO = "webdriver.gecko.driver";
    private static WebDriverBuilder instance = null;
    private WebDriverEventListener eventListener;
    private String executionMode = LocalPropertiesReader.getExecutionMode();
    private Logger log = LoggerFactory.getLogger(WebDriverBuilder.class);
    private String driverPath = System.getProperty("user.dir") + "/src/test/resources/browserdrivers/";

    private WebDriverBuilder() {
    }

    public static WebDriverBuilder getInstance() {
        if (instance == null) {
            instance = new WebDriverBuilder();
        }
        return instance;
    }

    public WebDriver build(String browserName) {
        WebDriver driver = instantiateDriver(browserName);
        driver = new EventFiringWebDriver(driver).register(eventListener);
        driver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT_TIME, TimeUnit.SECONDS);
        return driver;
    }

    public WebDriverBuilder eventListener(WebDriverEventListener eventListener) {
        this.eventListener = eventListener;
        return this;
    }

    private WebDriver instantiateDriver(String browserName) {
        WebDriver driver = null;

        if (executionMode.equalsIgnoreCase("local")) {
            driver = newLocalWebDriver(browserName);
        }
        else if (executionMode.equalsIgnoreCase("grid")) {
            driver = newRemoteWebDriver(browserName);
        }
        return driver;
    }

    private WebDriver newLocalWebDriver(String browserName) {
        WebDriver driver;

        OS os = OS.getOS();
        DesiredCapabilities cap = DesiredCapabilitiesFactory.makeDesiredCapabilitiesBrowsers(browserName);

        if (browserName.equalsIgnoreCase("firefox")) {
            String drivername;
            if (os == OS.WINDOWS) {
                drivername = "geckodriver-win-64.exe";
            }
            else if (os == OS.MAC) {
                drivername = "geckodriver-mac";
            }
            else {
                drivername = "geckodriver";
            }
            System.setProperty(PROPERTY_NAME_GECKO, getDriverPath(drivername));
            FirefoxOptions firefoxOptions = new FirefoxOptions(cap);
            driver = new FirefoxDriver(firefoxOptions);
            driver.manage().window().maximize();
        }
        else if (browserName.equalsIgnoreCase("chrome")) {
            String drivername;
            if (os == OS.WINDOWS) {
                drivername = "chromedriver-win.exe";
            }
            else if (os == OS.MAC) {
                drivername = "chromedriver-mac";
            }
            else {
                drivername = "chromedriver";
            }
            System.setProperty(PROPERTY_NAME_CHROME, getDriverPath(drivername));
            ChromeOptions chromeoptions = DesiredCapabilitiesFactory.createChromeOptions();
            driver = new ChromeDriver(chromeoptions);
            if (os == OS.WINDOWS) {
                driver.manage().window().maximize();
            }
            else {
                driver.manage().window().setSize(new Dimension(1800, 1800));

            }
        }
        else if (browserName.equalsIgnoreCase("edge")) {
            String edgeDriver = "MicrosoftWebDriver.exe";
            System.setProperty(PROPERTY_NAME_EDGE, getDriverPath(edgeDriver));
            EdgeOptions edgeOptions = DesiredCapabilitiesFactory.createEdgeOptions();
            driver = new EdgeDriver(edgeOptions);
            driver.manage().window().maximize();
        }
        else if (browserName.equalsIgnoreCase("ie")) {
            String drivername = "IEDriverServer-win32.exe";
            System.setProperty(PROPERTY_NAME_IE, getDriverPath(drivername));
            InternetExplorerOptions ieOptions = new InternetExplorerOptions(cap);
            driver = new InternetExplorerDriver(ieOptions);
            driver.manage().window().maximize();
        }
        else if (browserName.equalsIgnoreCase("safari")) {
            SafariOptions safariOptions = new SafariOptions(DesiredCapabilities.safari());
            driver = new SafariDriver(safariOptions);
            driver.manage().window().maximize();
        }
        else {
            throw new RuntimeException("This browser is not supported yet by the automation team");
        }
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_WAIT_TIME, TimeUnit.SECONDS);
        if (!browserName.equalsIgnoreCase("safari")) {
            driver.manage().timeouts().setScriptTimeout(SCRIPT_WAIT_TIME, TimeUnit.SECONDS);
        }
        return driver;
    }

    private String getDriverPath(String driverName) {
        String filePath = driverPath + driverName;
        return filePath;
    }

    private RemoteWebDriver newRemoteWebDriver(String browserName) {
        RemoteWebDriver remoteDriver;
        DesiredCapabilities cap = DesiredCapabilitiesFactory.makeDesiredCapabilitiesBrowsers(browserName);

        if (browserName.equalsIgnoreCase("firefox")) {
            String geckoDriverPath = "geckodriver";
            System.setProperty(PROPERTY_NAME_GECKO, getDriverPath(geckoDriverPath));
            FirefoxOptions firefoxOptions = new FirefoxOptions(cap);
            remoteDriver = new RemoteWebDriver(createGridRemoteAddress(), firefoxOptions);

            remoteDriver.manage().window().maximize();
        }
        else if (browserName.equalsIgnoreCase("chrome")) {
            String exeDriver = "chromedriver";
            System.setProperty(PROPERTY_NAME_CHROME, getDriverPath(exeDriver));
            ChromeOptions chromeoptions = DesiredCapabilitiesFactory.createChromeOptions();
            remoteDriver = new RemoteWebDriver(createGridRemoteAddress(), chromeoptions);
        }
        else if (browserName.equalsIgnoreCase("edge")) {
            String edgeDriver = "MicrosoftWebDriver.exe";
            System.setProperty(PROPERTY_NAME_EDGE, getDriverPath(edgeDriver));
            EdgeOptions edgeOptions = DesiredCapabilitiesFactory.createEdgeOptions();
            remoteDriver = new RemoteWebDriver(createGridRemoteAddress(), edgeOptions);
            remoteDriver.manage().window().maximize();
        }
        else if (browserName.equalsIgnoreCase("ie")) {
            String exeFilePath = "IEDriverServer-win32.exe";
            System.setProperty(PROPERTY_NAME_IE, getDriverPath(exeFilePath));
            InternetExplorerOptions ieOptions = new InternetExplorerOptions(cap);
            remoteDriver = new RemoteWebDriver(createGridRemoteAddress(), ieOptions);
            remoteDriver.manage().window().maximize();
        }
        else if (browserName.equalsIgnoreCase("safari")) {
            SafariOptions safariOptions = new SafariOptions(cap);
            remoteDriver = new RemoteWebDriver(createGridRemoteAddress(), safariOptions);
            remoteDriver.manage().window().maximize();
        }
        else {
            throw new RuntimeException("This browser is not supported yet by the automation team");
        }
        remoteDriver.setFileDetector(new LocalFileDetector());
        if (!browserName.equalsIgnoreCase("safari")) {
            remoteDriver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_WAIT_TIME, TimeUnit.SECONDS);
        }
        remoteDriver.manage().timeouts().setScriptTimeout(SCRIPT_WAIT_TIME, TimeUnit.SECONDS);
        return remoteDriver;
    }

    private URL createGridRemoteAddress() {
        URL remoteAddress = null;
        try {
            remoteAddress = new URL(LocalPropertiesReader.getGridUrl());
        }
        catch (MalformedURLException e) {
            log.error("Selenium Grid address is malformed", e);
            Assert.fail("Selenium Grid address is malformed", e);
        }
        return remoteAddress;
    }

}
