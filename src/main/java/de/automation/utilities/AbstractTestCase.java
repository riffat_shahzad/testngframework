package de.automation.utilities;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;
import org.slf4j.Logger;
import org.slf4j.MDC;

public abstract class AbstractTestCase extends AbstractWebDriverEventListener {

    abstract Logger getLogger();

    @Override
    public void beforeAlertAccept(WebDriver webDriver) {
    }

    @Override
    public void afterAlertAccept(WebDriver webDriver) {
    }

    @Override
    public void afterAlertDismiss(WebDriver webDriver) {
    }

    @Override
    public void beforeAlertDismiss(WebDriver webDriver) {
    }

    @Override
    public void beforeNavigateTo(String url, WebDriver driver) {
    }

    @Override
    public void beforeFindBy(By by, WebElement element, WebDriver driver) {
    }

    @Override
    public void afterFindBy(By by, WebElement element, WebDriver driver) {
        getLogger().debug("Found: " + by);
    }

    @Override
    public void onException(Throwable error, WebDriver driver) {
        getLogger().error("Exception has been triggered: " + error.getMessage());
    }

    @Override
    public void beforeClickOn(WebElement element, WebDriver driver) {
        getLogger().debug("Before clicking on: " + element);
    }

    @Override
    public void afterClickOn(WebElement element, WebDriver driver) {
        String testMethodName = MDC.get("testMethodName") + "_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH.mm.SS.SSS"));
        TestListener.createScreenshot(testMethodName, driver, false);
        getLogger().debug("After clicking on: " + element);
    }

    @Override
    public void beforeChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {
    }

    @Override
    public void afterChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {
    }

    @Override
    public void beforeScript(String script, WebDriver driver) {
        getLogger().debug("Preparing to execute: " + script);
    }

    @Override
    public void afterScript(String script, WebDriver driver) {
        getLogger().debug("Following script was executed: " + script);
    }

    @Override
    public void beforeNavigateBack(WebDriver driver) {
        getLogger().debug("Preparing to navigate back");
    }

    @Override
    public void beforeNavigateForward(WebDriver driver) {
        getLogger().debug("Preparing to navigate forward");
    }

    @Override
    public void afterNavigateBack(WebDriver driver) {
        getLogger().debug("Finished navigating back");
    }

    @Override
    public void afterNavigateForward(WebDriver driver) {
        getLogger().debug("Finished navigating forward");
    }

    @Override
    public void afterNavigateTo(String url, WebDriver driver) {
    }

    @Override
    public void afterNavigateRefresh(WebDriver arg0) {
    }

    @Override
    public void beforeNavigateRefresh(WebDriver arg0) {
    }
}
