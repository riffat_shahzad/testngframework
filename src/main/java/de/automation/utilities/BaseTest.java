package de.automation.utilities;

import java.util.Objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

@Listeners(TestListener.class)
public class BaseTest extends AbstractTestCase {

    private static ThreadLocal<WebDriver> threadInstanceWebDriver = new ThreadLocal<>();
    private final Boolean quitDriver = LocalPropertiesReader.getQuitDriverMode();
    protected Logger log = LoggerFactory.getLogger(this.getClass());
    private String url = LocalPropertiesReader.getUrl();

    // provide here browser name where you want to run the test like 'chrome', 'firefox', 'ie', 'edge', 'safari'

    @BeforeMethod(alwaysRun = true)
    @Parameters({"browserName"})
    public synchronized void createDriverInstance(@Optional("chrome") String browserName) {
        WebDriver driver = WebDriverBuilder.getInstance().eventListener(this).build(browserName);
        threadInstanceWebDriver.set(driver);
        driver.get(url);
    }

    @AfterMethod(alwaysRun = true)
    public synchronized void closeDriverInstance() {
        try {
            WebDriver driver = threadInstanceWebDriver.get();
            if (driver != null && quitDriver) {
                log.debug("Closing driver.");
                Objects.requireNonNull(driver).quit();
                log.debug("Driver has been closed");
            }
        }
        catch (WebDriverException e) {
            log.error("Error during closeBrowser" + e);
        }
    }

    public synchronized WebDriver getDriverInstance() {
        return threadInstanceWebDriver.get();
    }
    @Override
    Logger getLogger() {
        return log;
    }

}
