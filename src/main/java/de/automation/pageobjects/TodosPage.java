package de.automation.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TodosPage extends BasePage {
    WebDriver driver;
    private Logger log = LoggerFactory.getLogger(this.getClass());
    private By txtBxToDo = By.className("new-todo");
    private By listToDo = By.cssSelector(".todo-list li");
    private By countToDo = By.cssSelector(".todo-count strong");
    private By linkCompleted = By.linkText("Completed");
    private By linkActive = By.linkText("Active");
    private By linkAll = By.linkText("All");
    private By linkClearCompleted = By.className("clear-completed");

    public TodosPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public TodosPage enterTodo(String name) {
        sendText(txtBxToDo, name);
        getElement(txtBxToDo).sendKeys(Keys.ENTER);
        return this;
    }

    public int seeNumberOfTodos() {
        return getElements(listToDo).size();
    }

    public TodosPage filterCompleted() {
        click(linkCompleted);
        return this;
    }

    public TodosPage clearCompleted() {
        click(linkClearCompleted);
        return this;
    }


    public TodosPage filterActive() {
        click(linkActive);
        return this;
    }

    public TodosPage filterAll() {
        click(linkAll);
        return this;
    }

    public int getCountTodos() {
        return Integer.valueOf(getElement(countToDo).getText());
    }

    public TodosPage deleteNthTodo(int toDoNumber) {
        selectNthTodo(toDoNumber);
        driver.findElement(By.cssSelector(".todo-list li:nth-child(" + toDoNumber + ") button")).click();
        return this;
    }

    public TodosPage selectNthTodo(int toDoNumber) {
        By toDoRow = By.cssSelector(".todo-list li:nth-child(" + toDoNumber + ") input:nth-child(1)");
        driver.findElement(toDoRow).click();
        return this;
    }
}
