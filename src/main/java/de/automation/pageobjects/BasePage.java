package de.automation.pageobjects;

import java.time.Duration;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.automation.utilities.WebDriverBuilder;

public class BasePage {
    public Logger log = LoggerFactory.getLogger(this.getClass());
    private WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public BasePage click(By locator) {
        WebElement elementToClick = getElement(locator);
        elementToClick.click();
        log.info("Element with Locator = [" + locator + "] has been clicked.");
        return this;
    }

    public BasePage waitABit(long milis) {
        try {
            Thread.sleep(milis);
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }
        return this;
    }

    public WebElement getElement(By locator) {
        waitForElementVisibility(locator);
        return driver.findElement(locator);
    }

    public List<WebElement> getElements(By locator) {
        waitForElementVisibility(locator);
        return driver.findElements(locator);
    }

    public BasePage waitForElementVisibility(By locator) {
        new WebDriverWait(driver, 60).pollingEvery(Duration.ofMillis(500))
                .ignoring(NoSuchElementException.class)
                .ignoring(StaleElementReferenceException.class)
                .ignoring(WebDriverException.class)
                .until(ExpectedConditions.visibilityOfElementLocated(locator));
        log.info("Element is visible with following locator: " + locator);
        return this;
    }

    //Write Text
    public BasePage sendText(By locator, String text) {
        WebElement element = getElement(locator);
        element.sendKeys(text);
        log.info("locator = [" + locator + "], Text = [" + text + "] has been entered.");
        return this;
    }

    //Clear Text from any text field
    public BasePage clearText(By locator) {
        WebElement element = getElement(locator);
        element.clear();
        return this;
    }

    public boolean checkElementExist(By locator) {
        turnOffImplicitWait();
        boolean status = driver.findElements(locator).size() > 0;
        log.info("Size is: " + driver.findElements(locator).size());
        turnOnImplicitWait();
        return status;
    }

    private void turnOffImplicitWait() {
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        log.info("Temporary turning off the impliciWait");
    }

    private void turnOnImplicitWait() {
        driver.manage().timeouts().implicitlyWait(WebDriverBuilder.IMPLICIT_WAIT_TIME, TimeUnit.SECONDS);
        log.info("Setting back impliciWait to default value");
    }

    protected BasePage waitForIEAction(long millis) {
        waitABit(millis);
        return this;
    }

    public void scrollInToView(By locator) {
        WebElement element = getElement(locator);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        waitABit(300);
    }
}
